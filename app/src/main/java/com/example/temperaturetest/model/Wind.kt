package com.example.temperaturetest.model

data class Wind(
    val speed: Double,
    val deg: Int
)
