package com.example.temperaturetest.model

data class Forecast(
    val lat: Double,
    val lon: Double,
    val timezone: String,
    val timezone_offset: Int,
    val current: Current,
    val minutely: ArrayList<Minutely>,
    val hourly: ArrayList<Hourly>,
    val daily: ArrayList<Daily>,
    val alerts: ArrayList<Alerts>
)
