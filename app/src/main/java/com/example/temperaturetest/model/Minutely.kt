package com.example.temperaturetest.model

data class Minutely(
    val dt: Double,
    val precipitation: Double
)
