package com.example.temperaturetest.model

data class Current(
    val dt: Double,
    val sunrise: Double,
    val sunset: Double,
    val temp: Double,
    val feels_like: Double,
    val pressure: Double,
    val humidity: Double,
    val dew_point: Double,
    val uvi: Double,
    val clouds: Double,
    val visibility: Double,
    val wind_speed: Double,
    val wind_deg: Double,
    val weather: ArrayList<Weather>,
    val rain: Rain,
)

data class Rain(
    val h: Double
)
