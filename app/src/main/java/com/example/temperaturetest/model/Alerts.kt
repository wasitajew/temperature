package com.example.temperaturetest.model

data class Alerts(
    val sender_name: String,
    val event: String,
    val start: Double,
    val end: Double,
    val description: String,
    val tags: ArrayList<String>
)
