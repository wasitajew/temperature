package com.example.temperaturetest.model

data class Daily(
    val dt: Double,
    val sunrise: Double,
    val sunset: Double,
    val moonrise: Double,
    val moonset: Double,
    val moon_phase: Double,
    val temp: Temp,
    val feels_like: FeelLike,
    val pressure: Double,
    val humidity: Double,
    val dew_point: Double,
    val wind_speed: Double,
    val wind_deg: Double,
    val weather: ArrayList<Weather>,
    val clouds: Double,
    val pop: Double,
    val rain: Double,
    val uvi: Double
)

data class Temp(
    val day: Double,
    val min: Double,
    val max: Double,
    val night: Double,
    val eve: Double,
    val morn: Double
)

data class FeelLike(
    val day: Double,
    val night: Double,
    val eve: Double,
    val morn: Double
)
