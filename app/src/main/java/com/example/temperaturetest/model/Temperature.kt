package com.example.temperaturetest.model

data class Temperature(
    val coord: Coord,
    val weather: ArrayList<Weather>,
    val base: String,
    val main: Main,
    val visibility: Int,
    val wind: Wind,
    val clouds: Clouds,
    val dt: Int,
    val sys: SYS,
    val timeZone: Int,
    val id: Int,
    val name: String,
    val cod: Int
)

