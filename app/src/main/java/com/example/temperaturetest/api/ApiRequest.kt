package com.example.temperaturetest.api

import com.example.temperaturetest.model.Forecast
import com.example.temperaturetest.model.Temperature
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface ApiRequest {

    @GET("/data/2.5/weather?")
    fun getWeather(@Query("q") name: String, @Query("appid") appid : String): Call<Temperature>

    @GET("/data/2.5/onecall?")
    fun getForecast(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
        @Query("exclude") exclude: String,
        @Query("appid") appid : String
    ): Call<Forecast>

}