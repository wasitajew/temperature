package com.example.temperaturetest.mainFragment.adapter

import android.os.Build
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.example.temperaturetest.model.Hourly
import kotlinx.android.synthetic.main.item_forecast.view.*
import java.text.SimpleDateFormat
import java.util.*

class ViewHolder(itemsView: View): RecyclerView.ViewHolder(itemsView) {
    fun bind(hourly: Hourly) {

        val hour = Date(hourly.dt*1000L)
        val simpleDateFormat = SimpleDateFormat("dd-MM-yyyy' 'HH:mm")

        itemView.textViewHour.text = simpleDateFormat.format(hour)
        itemView.textViewMain.text = hourly.weather[0].main
        itemView.textViewDescription.text = hourly.weather[0].description

    }
}