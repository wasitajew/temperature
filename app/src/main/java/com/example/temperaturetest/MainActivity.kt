package com.example.temperaturetest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.temperaturetest.mainFragment.ForecastFragment
import com.example.temperaturetest.mainFragment.TemperatureFragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {

    private val temperatureFragment = TemperatureFragment()
    private val forecastFragment = ForecastFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        replaceFragment(temperatureFragment)

        val bottomNavigationView = findViewById<BottomNavigationView>(R.id.bottomNavigationView)

        bottomNavigationView.setOnItemSelectedListener {
            when (it.itemId){
                R.id.item_temperature -> replaceFragment(temperatureFragment)
                R.id.item_forecast -> replaceFragment(forecastFragment)
                else -> replaceFragment(temperatureFragment)
            }
        }

    }

    private fun replaceFragment(fragment: Fragment): Boolean {
        if (fragment != null) {
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.container, fragment)
            transaction.commit()
        }
        return true
    }
}